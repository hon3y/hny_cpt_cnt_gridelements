[userFunc = TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('gridelements')]
    <INCLUDE_TYPOSCRIPT: source="FILE:EXT:gridelements/Configuration/TypoScript/setup.ts" extensions="ts">
[global]

tt_content.gridelements_pi1 =< lib.contentElement
tt_content.gridelements_pi1 {
    templateName = Generic
    variables {
        content =< tt_content.gridelements_view
    }
}

tt_content.gridelements_pi1 = COA
tt_content.gridelements_pi1.20.10.setup {
    1 < lib.gridelements.defaultGridSetup
    1 {
        cObject = FLUIDTEMPLATE
        cObject {
            format = html
            file = EXT:hny_cpt_cnt_gridelements/Resources/Private/GridElements/Templates/Bootstrap/1Column.html
        }
    }

    2 < lib.gridelements.defaultGridSetup
    2 {
        cObject = FLUIDTEMPLATE
        cObject {
            format = html
            file = EXT:hny_cpt_cnt_gridelements/Resources/Private/GridElements/Templates/Bootstrap/2Columns.html
        }
    }

    3 < lib.gridelements.defaultGridSetup
    3 {
        cObject = FLUIDTEMPLATE
        cObject {
            format = html
            file = EXT:hny_cpt_cnt_gridelements/Resources/Private/GridElements/Templates/Bootstrap/3Columns.html
        }
    }

    4 < lib.gridelements.defaultGridSetup
    4 {
        cObject = FLUIDTEMPLATE
        cObject {
            format = html
            file = EXT:hny_cpt_cnt_gridelements/Resources/Private/GridElements/Templates/Bootstrap/4Columns.html
        }
    }

    5 < lib.gridelements.defaultGridSetup
    5 {
        cObject = FLUIDTEMPLATE
        cObject {
            format = html
            file = EXT:hny_cpt_cnt_gridelements/Resources/Private/GridElements/Templates/Bootstrap/6Columns.html
        }
    }

    6 < lib.gridelements.defaultGridSetup
    6 {
        cObject = FLUIDTEMPLATE
        cObject {
            format = html
            file = EXT:hny_cpt_cnt_gridelements/Resources/Private/GridElements/Templates/Bootstrap/Wrapper.html
        }
    }
}

tt_content.gridelements_view < tt_content.gridelements_pi1